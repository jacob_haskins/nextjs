// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

export default async (req, res) => {
  const data = await fetch(
    "https://www.multistate.us/research/covid/ahla.json?key=eea87ae3f735ce2eff87f33ab7bd3996"
  );
  const json = await data.json();

  const AsyncAirtable = require("asyncairtable");
  const asyncAirtable = new AsyncAirtable(
    "keyKQKhCGtSsDszRl",
    "appCFZHYspmyIgeB9"
  );

  const results = await asyncAirtable.select("Multistate Data");

  for await (const record of results) {
    const jsonRecord = json.find(
      (d) => d.Jurisdiction === record.fields.Jurisdiction
    );

    if (jsonRecord) {
      asyncAirtable.updateRecord("Multistate Data", {
        id: record.id,
        fields: { ...jsonRecord },
      });
    } else {
      asyncAirtable.createRecord("Multistate Data", {
        id: record.id,
        fields: { ...jsonRecord },
      });
    }
  }

  res.statusCode = 200;
  res.json({ message: "Data Updated" });
};
