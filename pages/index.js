import Head from "next/head";
import React from "react";
import styles from "../styles/Home.module.css";
import { makeStyles, withTheme } from "@material-ui/core/styles";
import ReactMarkdown from "react-markdown";
import TextField from "@material-ui/core/TextField";
import ToggleButton from "@material-ui/lab/ToggleButton";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";

const useStyles = makeStyles({
  root: {
    width: "100%",
  },
  header: {
    marginTop: 14,
  },
  container: {
    maxHeight: 800,
  },
  toggle: {
    backgroundColor: "#9F2E32",
  },
});

export async function getServerSideProps(context) {
  var Airtable = require("airtable");
  Airtable.configure({
    endpointUrl: "https://api.airtable.com",
    apiKey: "keyKQKhCGtSsDszRl",
  });
  var base = Airtable.base("appCFZHYspmyIgeB9");
  var _ = require("lodash");

  let noteRecords = [];
  let dataRecords = [];

  // Fetch AHLA managed notes
  await base("AHLA Notes")
    .select()
    .all()
    .then((records) => {
      noteRecords = records.map((d) => d.fields);
    })
    .catch((err) => {
      console.error(err);
    });

  // Fetch data from Multistate API
  await base("Multistate Data")
    .select()
    .all()
    .then((records) => {
      dataRecords = records.map((d) => d.fields);
    })
    .catch((err) => {
      console.error(err);
    });

  var data = _.map(dataRecords, function (item) {
    return _.merge(
      item,
      _.find(noteRecords, { Jurisdiction: item.Jurisdiction })
    );
  });

  return {
    props: { data }, // will be passed to the page component as props
  };
}

export default function Home(props) {
  const classes = useStyles();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [locale, setLocale] = React.useState("State");

  const columns = [
    {
      id: "Jurisdiction",
      label: "Jurisdiction",
      minWidth: 170,
      format: (d) => (
        <a target="_blank" href={d["Source 1"]}>
          {d.Jurisdiction}
        </a>
      ),
    },
    {
      id: "Summary",
      label: "Vaccination Plans",
      format: (d) => (
        <ReactMarkdown linkTarget="_blank">{d.Summary}</ReactMarkdown>
      ),
    },
    {
      id: "Additional Information",
      label: "Additional Information",
      minWidth: 300,
      format: (d) => (
        <ReactMarkdown linkTarget="_blank">
          {d["Additional Information"]}
        </ReactMarkdown>
      ),
    },
  ];

  const handleLocaleChange = (event, newLocale) => {
    setLocale(newLocale);
  };

  var _ = require("lodash");
  let data = props.data.filter((d) => d.Level === locale);
  data = _.orderBy(data, ["Jurisdiction"], ["asc"]);

  return (
    <div className={styles.container}>
      <div className={classes.header}>
        <ToggleButtonGroup
          value={locale}
          exclusive
          onChange={handleLocaleChange}
          aria-label="set locale"
          className={classes.toggle}
        >
          <ToggleButton value="State" style={{ color: "#fff" }}>
            State
          </ToggleButton>
          <ToggleButton value="Local" style={{ color: "#fff" }}>
            Local
          </ToggleButton>
        </ToggleButtonGroup>
      </div>

      <Paper className={classes.root}>
        <TableContainer className={classes.container}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                {columns.map((column) => (
                  <TableCell
                    key={column.id}
                    align={column.align}
                    style={{ minWidth: column.minWidth }}
                  >
                    {column.label}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {data.map((row) => {
                return (
                  <TableRow
                    hover
                    role="checkbox"
                    tabIndex={-1}
                    key={row.Jurisdiction}
                  >
                    {columns.map((column) => {
                      const value = row[column.id];
                      return (
                        <TableCell
                          key={column.id}
                          align={column.align}
                          style={{ verticalAlign: "top" }}
                        >
                          {column.format ? column.format(row) : value}
                        </TableCell>
                      );
                    })}
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>
      </Paper>
    </div>
  );
}
